package com.example.myapplicationfranklinslaby.app;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class QuestInfo extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quest_info);

        Intent intent = getIntent();
        String type = intent.getStringExtra(Activity2.QUEST_INFO);
        String info;

        //receive info about bandits
        if(type == "bandit"){
            info = "Bandits in the Woods:\n" +
                    "\n" +
                    " alignment: GOOD,\n" +
                    " description:\n" +
                    " \"The famed bounty hunter HotDog has requested\n" +
                    "   the aid of a hero in ridding the woods of terrifying\n" +
                    "   bandits who have thus far eluded his capture, as he\n" +
                    "   is actually a dog, and cannot actually grab things\n" +
                    "   more than 6 feet off the ground.\n" +
                    "        location: (46.908588, -96.808991)\n" +
                    "        QuestGiver: HotDogg The Bounty Hunter (46.8541979, -96.8285138)";

            //create the text
            TextView textView = new TextView(this);
            textView.setTextSize(20);
            textView.setText(info);

            //show quest info
            setContentView(textView);
        }

         //receive info about special delivery
         if(type == "special"){
            info = "Special Delivery:\n" +
                    "\n" +
                    " alignment: NEUTRAL,\n" +
                    " description:\n" +
                    " \"Sir Jimmy was once the fastest man in the kingdom, brave\n" +
                    "   as any soldier and wise as a king. Unfortunately, age\n" +
                    "   catches us all in the end, and he has requested that\n" +
                    "   I, his personal scribe, find a hero to deliver a package\n" +
                    "   of particular importance--and protect it with their life.\n" +
                    "        location: (46.8657639, -96.7363173)\n" +
                    "        QuestGiver: Sir Jimmy The Swift (46.8739748, -96.806112)";

             //create the text
             TextView textView = new TextView(this);
             textView.setTextSize(20);
             textView.setText(info);

             //show quest info
             setContentView(textView);
         }

        //receive info about mongrel
        if(type == "mongrel"){
            info = "Filthy Mongrel:\n" +
                    "\n" +
                    " alignment: EVIL,\n" +
                    " description:\n" +
                    " \"That strange dog that everyone is treating like a bounty-hunter\n" +
                    "   must go. By the order of Prince Jack, that smelly, disease ridden\n" +
                    "   mongrel must be removed from our streets by any means necessary. \n" +
                    "   He is disrupting the lives of ordinary citizens, and it's just\n" +
                    "   really weird. Make it gone.\n" +
                    "        location: (46.892386,-96.799669)\n" +
                    "        QuestGiver: Prince Jack, The Iron Horse (46.8739748, -96.806112)";

            //create the text
            TextView textView = new TextView(this);
            textView.setTextSize(20);
            textView.setText(info);

            //show quest info
            setContentView(textView);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.quest_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
