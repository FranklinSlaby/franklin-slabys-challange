package com.example.myapplicationfranklinslaby.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    // variables for name and password
    public String name = "Lancelot";
    public String password = "arthurDoesntKnow" ;
    public String resetName = "Try again";
    public String resetPassword = "";


    //make the submit button do something when clicked
    public void submitInformation (View view){

        //get the name submitted into the text box and set it as a string
        EditText editText = (EditText) findViewById(R.id.editName);
        String checkName = editText.getText().toString();

        //get the password submitted into the password text field and set
        //it as a string
        EditText editPassword = (EditText) findViewById(R.id.editPassword);
        String checkPassword = editPassword.getText().toString();

        //check both the password and the name entered to the expected
        //values
        if(name == checkName && password == checkPassword){
            Intent intent = new Intent(this,Activity2.class);
            startActivity(intent);
        }

        //If wrong display a message in the name box to "Try again"
        else{

        }

    }

}
